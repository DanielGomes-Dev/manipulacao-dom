(function run() {



    const renderDiv = document.getElementById('render');
    let renderMessage = [];

    function read() {

        const nameForm = document.getElementById('name');
        const messageForm = document.getElementById('message');

        const name = nameForm.value;
        const message = messageForm.value;
        renderMessage.unshift({ name, message });

        return

    }

    function createElementCard(message, index) {
        // Criar Card
        const card = document.createElement('div');
        card.classList.add('card')

        // Header Card
        const cardHeader = document.createElement('div');
        cardHeader.classList.add('cardHeader')

        const h1Name = document.createElement('h1');
        h1Name.innerText = message.name;
        cardHeader.appendChild(h1Name);

        // Body Card
        const cardBody = document.createElement('div');
        cardBody.classList.add('cardBody');

        const pMessage = document.createElement('p');
        pMessage.innerText = message.message;
        cardBody.appendChild(pMessage);

        // Footer Card
        const cardFooter = document.createElement('div');
        cardFooter.classList.add('cardFooter');

        const buttonEdit = document.createElement('button');
        buttonEdit.id = (`${index}`);
        buttonEdit.classList.add('edit');
        buttonEdit.innerText = 'Editar';
        cardFooter.appendChild(buttonEdit);

        const buttonDelete = document.createElement('button');
        buttonDelete.id = (`${index}`)
        buttonDelete.classList.add('delete');
        buttonDelete.innerText = 'Deletar';
        cardFooter.appendChild(buttonDelete);

        card.appendChild(cardHeader);
        card.appendChild(cardBody);
        card.appendChild(cardFooter);

        return card
    }



    function renderElement() {
        renderDiv.innerHTML = null;
        renderMessage.forEach((message, index) => {

            const card = createElementCard(message, index);
            renderDiv.append(card);

        });
        runEditDelete();
    }




    function runEditDelete() {
        const buttonCardEdit = document.querySelectorAll('.edit');
        const buttonCardDelete = document.querySelectorAll('.delete');

        buttonCardDelete.forEach(button => {

            button.addEventListener('click', e => {
                renderMessage.splice(button.id, 1)
                if (!confirm('Delete')) return;
                renderElement();
            });

        });

        buttonCardEdit.forEach(button => {

            button.addEventListener('click', e => {
                console.log(renderMessage[button.id])

            })

        })

    };




    function deleteAll() {
        const buttonDeleteAll = document.getElementById('deleteAll');

        buttonDeleteAll.addEventListener('click', e => {
            renderMessage = [];
            renderDiv.innerHTML = null;


        })
    }






    function render() {
        const form = document.getElementById('form');

        form.addEventListener('submit', e => {
            e.preventDefault();
            read();
            renderElement();

        })
    }


    render();
    deleteAll();


})();